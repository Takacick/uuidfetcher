package net.opgroup.uuidfetcher.database;

import lombok.SneakyThrows;
import net.opgroup.uuidfetcher.UUIDFetcher;

import java.sql.ResultSet;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class PlayerDatabase {

    /**
     * @param key    section
     * @param object value for the key
     * @param result section which should be returned
     * @return the searched object or null
     */
    @SneakyThrows
    public static Future<Object> get(String key, Object object, String result) {
        return Executors.newCachedThreadPool().submit(() -> {
            ResultSet resultSet = UUIDFetcher.getInstance().getMySQL()
                    .getResult("SELECT " + result + " FROM Players WHERE " + key + "= '" + object + "'");
            if (resultSet.next()) {
                return resultSet.getObject(result);
            }

            return null;
        });
    }

}
