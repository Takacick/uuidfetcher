package net.opgroup.uuidfetcher.database;


import lombok.Getter;

import java.sql.*;

public class MySQL {

    // instance variables
    @Getter
    private Connection connection;
    private String host;
    private String port;
    private String database;
    private String username;
    private String password;

    /**
     * creates a new mysql object (does not yet establish a connection)
     *
     * @param host     the hostname of the server
     * @param port     the port of the sql server
     * @param database the name of the database
     * @param username the username
     * @param password the corresponding password
     */
    public MySQL(String host, String port, String database, String username, String password) {
        // initialising instance variables
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
        this.connection = null;
    }

    /**
     * establishes the connection to the sql database
     */
    public void connect() {
        try {
            this.connection = DriverManager.getConnection(
                    "jdbc:mysql://" + host + ":" + port + "/" + database + "?autoReconnect=true", username, password);
            System.out.println("[UUIDFetcher] Database connection established");
        } catch (SQLException exception) {
            System.out.println("[UUIDFetcher] Could not connect to database service!");
        }
    }

    /**
     * closes existing sql connection
     */
    public void disconnect() {
        try {
            this.connection.close();
            this.connection = null;
            System.out.println("[UUIDFetcher] Disconnected from database service");
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * executes an update
     *
     * @param query the statement for the update
     */
    public void update(String query) {
        if (this.connection != null) {
            try {
                connection.createStatement().executeUpdate(query);
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
    }

    /**
     * prepares a statement
     *
     * @param query the contents of the statement
     * @return the prepared statement
     */
    public PreparedStatement prepare(String query) {
        if (this.connection != null) {
            try {
                return this.connection.prepareStatement(query);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        return null; // if something goes wrong
    }


    /**
     * @param query the statement for the result
     * @return the corresponding result
     */
    public ResultSet getResult(String query) {
        if (this.connection != null) {
            try {
                return connection.createStatement().executeQuery(query);
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }
}