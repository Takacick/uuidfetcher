package net.opgroup.uuidfetcher.configuration;

import com.google.common.collect.Maps;
import lombok.Getter;
import net.opgroup.uuidfetcher.UUIDFetcher;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class MainConfiguration {

    // instance variables
    @Getter
    private File file;
    private FileConfiguration fileConfiguration;
    private String settingsPath;
    private Map<String, String> settingsCache;

    /**
     * Main configuration and caching, constructs new object of this class
     */
    public MainConfiguration() {
        // initialising instance variables
        this.file = new File(UUIDFetcher.getInstance().getDataFolder() + "/config.yml");
        this.fileConfiguration = YamlConfiguration.loadConfiguration(file);
        this.settingsPath = "Settings";
        this.settingsCache = Maps.newHashMap();

        // default settings
        this.fileConfiguration.addDefault(this.settingsPath + ".sqlHost", "localhost");
        this.fileConfiguration.addDefault(this.settingsPath + ".sqlPort", "3306");
        this.fileConfiguration.addDefault(this.settingsPath + ".sqlUsername", "root");
        this.fileConfiguration.addDefault(this.settingsPath + ".sqlPassword", "password");
        this.fileConfiguration.addDefault(this.settingsPath + ".sqlDatabase", "database");

        // configuration option
        this.fileConfiguration.options().copyDefaults(true);

        // saving file configuration
        try {
            this.fileConfiguration.save(this.file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // caching messages
        this.cache();
    }

    /**
     * caches all sections from configuration file
     */
    public void cache() {
        // clearing cache
        this.fileConfiguration = YamlConfiguration.loadConfiguration(file);
        this.settingsCache.clear();

        // caching settings from config
        for (String setting : this.fileConfiguration.getConfigurationSection(this.settingsPath).getKeys(false)) {
            this.settingsCache.put(setting, this.fileConfiguration.getString(this.settingsPath + "." + setting));
        }
    }

    /**
     * @param key the key of the setting
     * @return the cached setting to the given key
     */
    public String getSetting(String key) {
        return this.settingsCache.get(key);
    }

}
