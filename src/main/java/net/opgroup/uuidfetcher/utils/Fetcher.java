package net.opgroup.uuidfetcher.utils;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.SneakyThrows;
import net.opgroup.uuidfetcher.UUIDFetcher;
import net.opgroup.uuidfetcher.database.PlayerDatabase;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Fetcher {

    /**
     * cache which saves all fetched uuids for 20 minutes
     */
    private static LoadingCache<String, UUID> uuidCache = CacheBuilder.newBuilder()
            .expireAfterWrite(20, TimeUnit.MINUTES)
            .build(new CacheLoader<String, UUID>() {
                @SneakyThrows
                @Override
                public UUID load(String key) throws Exception {
                    // searching for the uuid in our database
                    String result = (String) PlayerDatabase.get("Name", key, "UUID").get();
                    UUID uuid;
                    if (result == null) {
                        // fetching the uuid directly from mojang
                        uuid = getUUIDFromMojang(key).get();
                    } else {
                        uuid = UUID.fromString(result);
                    }

                    return uuid;
                }
            });

    /**
     * cache which saves all fetched names for 20 minutes
     */
    private static LoadingCache<UUID, String> nameCache = CacheBuilder.newBuilder()
            .expireAfterWrite(20, TimeUnit.MINUTES)
            .build(new CacheLoader<UUID, String>() {
                @SneakyThrows
                @Override
                public String load(UUID key) throws Exception {

                    // searching for the name in our database
                    String name = (String) PlayerDatabase.get("UUID", key, "Name").get();
                    if (name == null) {
                        // fetching the name directly from mojang
                        name = getNameFromMojang(key).get();
                    }

                    return name;
                }
            });

    /**
     * fetches the uuid for the given name
     *
     * @param name
     * @return current name or null if the name does not exist
     */
    @SneakyThrows
    public static UUID getUUID(String name) {
        return uuidCache.get(name);
    }

    /**
     * fetches the name for the given uuid
     *
     * @param uuid
     * @return current name or null if the uuid does not exist
     */
    @SneakyThrows
    public static String getName(UUID uuid) {
        return nameCache.get(uuid);
    }

    // source code under this comment is for fetching informations directly from mojang!

    /**
     * Fetches the uuid asynchronously for a specified name and time
     *
     * @param name The name
     */
    private static Future<UUID> getUUIDFromMojang(final String name) {
        return Executors.newCachedThreadPool().submit(() -> {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(String.format("https://api.mojang.com/users/profiles/minecraft/%s?at=%d", name.toLowerCase(), System.currentTimeMillis() / 1000)).openConnection();
                connection.setReadTimeout(5000);

                // Convert to a JSON object to print data
                JsonParser jp = new JsonParser(); //from gson
                JsonElement element = jp.parse(new InputStreamReader((InputStream) connection.getContent())); //Convert the input stream to a json element
                JsonObject json = element.getAsJsonObject(); //May be an array, may be an object.
                UUID uuid = UUID.fromString(json.get("id").getAsString().replaceFirst("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5"));


                nameCache.put(uuid, json.get("name").getAsString());

                return uuid;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    /**
     * Fetches the name asynchronously and returns it
     *
     * @param uuid The uuid
     * @return The name
     */
    private static Future<String> getNameFromMojang(UUID uuid) {
        return Executors.newCachedThreadPool().submit(() -> {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(String.format("https://api.mojang.com/user/profiles/%s/names", UUIDTypeAdapter.fromUUID(uuid))).openConnection();
                connection.setReadTimeout(5000);

                // Convert to a JSON object to print data
                JsonParser jp = new JsonParser(); //from gson
                JsonElement element = jp.parse(new InputStreamReader((InputStream) connection.getContent())); //Convert the input stream to a json element

                JsonArray array = element.getAsJsonArray();
                JsonObject json = array.get(array.size() - 1).getAsJsonObject();

                uuidCache.put(json.get("name").getAsString(), uuid);

                return json.get("name").getAsString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        });
    }

}
