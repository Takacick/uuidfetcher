package net.opgroup.uuidfetcher;

import lombok.Getter;
import lombok.SneakyThrows;
import net.opgroup.uuidfetcher.configuration.MainConfiguration;
import net.opgroup.uuidfetcher.database.MySQL;
import net.opgroup.uuidfetcher.utils.Fetcher;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

@Getter
public class UUIDFetcher extends JavaPlugin {

    // instance variables
    private static UUIDFetcher instance;
    private MainConfiguration mainConfiguration;
    private MySQL mySQL;

    /**
     * On server load (called before {@link #onEnable()})
     */
    @Override
    public void onLoad() {
        // initialising instance class
        instance = this;
    }

    /**
     * On server start up, after {@link #onLoad()}
     */
    @Override
    public void onEnable() {
        // setting up config and corresponding cache
        this.mainConfiguration = new MainConfiguration();
        // creating and/or loading configuration file
        if (!this.mainConfiguration.getFile().exists())
            this.saveDefaultConfig();

        // setting up config and corresponding cache
        this.mainConfiguration = new MainConfiguration();
        // creating and/or loading configuration file
        if (!this.mainConfiguration.getFile().exists())
            this.saveDefaultConfig();

        // establishing sql connection
        this.mySQL = new MySQL(this.mainConfiguration.getSetting("sqlHost"), this.mainConfiguration.getSetting("sqlPort"),
                this.mainConfiguration.getSetting("sqlDatabase"), this.mainConfiguration.getSetting("sqlUsername"),
                this.mainConfiguration.getSetting("sqlPassword"));
        this.mySQL.connect();
        // database reconnect to not loose connection
        this.reconnect();
    }

    /**
     * On server shutdown
     */
    @Override
    public void onDisable() {
        // properly disconnecting from sql service if connected
        if (this.mySQL.getConnection() != null)
            this.mySQL.disconnect();
    }

    /**
     * reconnects every 3 minutes to sql database
     */
    private void reconnect() {
        Bukkit.getScheduler().runTaskTimerAsynchronously(
                this, () -> {
                    // refreshing database connection
                    this.mySQL.disconnect();
                    this.mySQL.connect();
                }, (20 * 60) * 3, (20 * 60) * 3); // 3 minute interval
    }

    /**
     * @return the main instance of this plugin
     */
    public static UUIDFetcher getInstance() {
        return instance;
    }

    /**
     * fetches the name for the given uuid
     *
     * @param uuid
     * @return current name or null if the uuid does not exist
     */
    @SneakyThrows
    public static String getName(UUID uuid) {
        return Fetcher.getName(uuid);
    }

    /**
     * fetches the uuid for the given name
     *
     * @param name
     * @return current name or null if the name does not exist
     */
    @SneakyThrows
    public static UUID getUUID(String name) {
        return Fetcher.getUUID(name);
    }

}
